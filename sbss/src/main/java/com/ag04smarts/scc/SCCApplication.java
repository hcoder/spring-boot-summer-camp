package com.ag04smarts.scc;

/*import com.ag04smarts.scc.controllers.CandidateController;
import com.ag04smarts.scc.controllers.RecipeController;
import com.ag04smarts.scc.controllers.TestController;
import com.ag04smarts.scc.schoolUrlBeans.*; */
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
//@ImportResource("classpath:lunchRecipes-configuration.xml")
public class SCCApplication {

    public static void main(String[] args) {

        ApplicationContext ctx = SpringApplication.run(SCCApplication.class, args);

      /*  System.out.println(ctx.getBean(TestController.class).sayHello());
        System.out.println(ctx.getBean(CandidateController.class).sayHello());
        System.out.println(ctx.getBean(RecipeController.class).showRecipes());
        System.out.println(ctx.getBean(FacebookUrl.class).getUrl());
        System.out.println(ctx.getBean(LinkedInUrl.class).getUrl());
        System.out.println(ctx.getBean(TwitterUrl.class).getUrl());
        System.out.println(ctx.getBean(SCname.class).getName());
        System.out.println(ctx.getBean(HC.class).getApology()); // thirds the charm */
    }
}
