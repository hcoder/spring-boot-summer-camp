package com.ag04smarts.scc;


import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:application.yml")

public class TestBean implements DisposableBean, InitializingBean {

    private final String hc;
    private final String xy;

    @Autowired
    public TestBean(@Value("${hc.excuses.apologies}") String hc,
                    @Value("${test.someString}") String xy) {
        this.hc = hc;
        this.xy = xy;
        System.out.println(xy);
    }

    //Bean initialization code
    @Override
    public void afterPropertiesSet() {
        System.out.println("Initializing method of HC TestBean bean is invoked!");
    }

    //Bean destruction code
    @Override
    public void destroy() {
        System.out.println("Destroy method of HC TestBean bean is invoked!");
    }
}
