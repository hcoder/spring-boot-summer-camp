package com.ag04smarts.scc.repositories;

import com.ag04smarts.scc.models.Course;
import org.springframework.data.repository.CrudRepository;

public interface CourseRepository extends CrudRepository<Course, Long> {
}
