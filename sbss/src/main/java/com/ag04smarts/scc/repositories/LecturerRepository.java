package com.ag04smarts.scc.repositories;

import com.ag04smarts.scc.models.Lecturer;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface LecturerRepository extends CrudRepository<Lecturer, Long> {

    Optional<Lecturer> findByFirstNameAndLastName(String name, String surname);
}
