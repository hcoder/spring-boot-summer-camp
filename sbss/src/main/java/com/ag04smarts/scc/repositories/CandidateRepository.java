package com.ag04smarts.scc.repositories;

import com.ag04smarts.scc.models.Candidate;
import org.springframework.data.repository.CrudRepository;

public interface CandidateRepository extends CrudRepository<Candidate, Long> {
}



