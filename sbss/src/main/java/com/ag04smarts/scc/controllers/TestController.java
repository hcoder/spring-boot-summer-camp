package com.ag04smarts.scc.controllers;

import com.ag04smarts.scc.services.CandidateService;
import org.springframework.stereotype.Controller;

@Controller
public class TestController {

    private CandidateService candidateService;

    public TestController(CandidateService candidateService) {
        this.candidateService = candidateService;
    }

    public String sayHello() {
        return candidateService.sayHello();
    }
}