package com.ag04smarts.scc.controllers;


import com.ag04smarts.scc.exceptions.RestPreconditions;
import com.ag04smarts.scc.models.Candidate;
import com.ag04smarts.scc.services.CandidateService;
import org.assertj.core.util.Preconditions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

/*
@RestController
@RequestMapping("/api/candidate")
public class CandidateController {


    private CandidateService candidateService;

    @Autowired
    public CandidateController(@Qualifier("candidateServiceImpl") CandidateService candidateService) {
        this.candidateService = candidateService;
    }

    @GetMapping
    public Iterable<Candidate> findAllCandidates() {
        return candidateService.listAllCandidates();
    }


    @GetMapping(value = "/{id}")
    public Candidate findCandidateById(@PathVariable("id") Long id) {
        return RestPreconditions.checkFound(candidateService.getCandidateById(id));
    }


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Candidate createCandidate(Candidate candidate) {
        Preconditions.checkNotNull(candidate);
        return candidateService.saveCandidate(candidate);
    }


    @PutMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void updateCandidate(@PathVariable("id") Long id, Candidate candidate) {
        Preconditions.checkNotNull(candidate);
        RestPreconditions.checkFound(candidateService.getCandidateById(id));
        candidateService.saveCandidate(candidate);
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable("id") Long id) {
        candidateService.deleteCandidate(id);
    }


    public String sayHello() {
        System.out.println("Hello! :)");

        return candidateService.sayHello();
    }
} */




