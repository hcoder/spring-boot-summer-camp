package com.ag04smarts.scc.controllers;


import com.ag04smarts.scc.services.RecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class RecipeController {

    private RecipeService recipeService;

    @Autowired
    public RecipeController(RecipeService recipeService) {
        this.recipeService = recipeService;
    }

    public String showRecipes() {
        return recipeService.getRecipes();
    }
}
