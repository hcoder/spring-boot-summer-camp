package com.ag04smarts.scc.configurations;

import com.ag04smarts.scc.models.Recipes;
import com.ag04smarts.scc.services.RecipeService;
import com.ag04smarts.scc.services.RecipesServiceFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

@Configuration
public class RecipesConfiguration {

    @Bean
    public Recipes recipes() {
        return new Recipes();
    }

    @Bean
    public String dinnerRecipes() {
        return "";
    }

  /*  @Bean
    public RecipeService recipeService() {
        return new RecipeService() {
            @Override
            public String getRecipes() {
                return null;
            }
        };
    }

    @Bean
    RecipesServiceFactory recipesServiceFactory(Recipes recipes) {
        return new RecipesServiceFactory((recipes));
    }

    @Bean
    @Primary
    @Profile({"default", "H1"})
    RecipesServiceFactory recipesServiceImpl(RecipesServiceFactory recipesServiceFactory) {
        return (RecipesServiceFactory) recipesServiceFactory.createRecipeService("H1");
    }

    @Bean
    @Profile("H2")
    RecipesServiceFactory lunchRecipeService(RecipesServiceFactory recipesServiceFactory) {
        return (RecipesServiceFactory) recipesServiceFactory.createRecipeService("H2");
    }

   */
}
