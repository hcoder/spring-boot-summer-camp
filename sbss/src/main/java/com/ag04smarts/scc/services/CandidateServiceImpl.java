package com.ag04smarts.scc.services;

import com.ag04smarts.scc.models.Candidate;
import com.ag04smarts.scc.repositories.CandidateRepository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class CandidateServiceImpl implements CandidateService {

    private CandidateRepository candidateRepository;

    public CandidateServiceImpl(@Qualifier("candidateRepository") CandidateRepository candidateRepository) {
        this.candidateRepository = candidateRepository;
    }

    @Override
    public Iterable<Candidate> listAllCandidates() {
        return candidateRepository.findAll();
    }

    @Override
    public Candidate getCandidateById(Long id) {
        return candidateRepository.findById(id).orElse(null);
    }

    @Override
    public Candidate saveCandidate(Candidate candidate) {
        return candidateRepository.save(candidate);
    }

    @Override
    public void deleteCandidate(Long id) {
        candidateRepository.deleteById(id);
    }


    private static final String Hello_candidates = "Hello candidates!";

    @Override
    public String sayHello() {
        return Hello_candidates;
    }

}


