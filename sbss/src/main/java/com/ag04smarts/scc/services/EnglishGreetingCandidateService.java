package com.ag04smarts.scc.services;

import com.ag04smarts.scc.models.Candidate;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Primary
@Profile({"en", "default"})
public class EnglishGreetingCandidateService implements CandidateService {


    @Override
    public Iterable<Candidate> listAllCandidates() {
        return null;
    }

    @Override
    public Candidate getCandidateById(Long id) {
        return null;
    }

    @Override
    public Candidate saveCandidate(Candidate candidate) {
        return null;
    }

    @Override
    public void deleteCandidate(Long id) {

    }

    @Override
    public String sayHello() {
        return "Hello boys and girls!";
    }
}