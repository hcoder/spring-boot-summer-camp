package com.ag04smarts.scc.services;

import com.ag04smarts.scc.models.Recipes;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Profile("H2")
public class LunchRecipeService implements RecipeService {

    private final Recipes recipes;

    public LunchRecipeService(Recipes recipes) {
        this.recipes = recipes;
    }

    @Override
    public String getRecipes() {
        return recipes.getRandomLunchRecipes();
    }
}
