package com.ag04smarts.scc.services;

import com.ag04smarts.scc.models.Recipes;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Primary
@Profile({"H1", "default"})
public class RecipeServiceImpl implements RecipeService {

    private final Recipes recipes;

    public RecipeServiceImpl(Recipes recipes) {
        this.recipes = recipes;
    }

    @Override
    public String getRecipes() {
        return recipes.getRandomRecipes();
    }
}
