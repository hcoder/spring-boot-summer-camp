package com.ag04smarts.scc.exceptions;


public class CandidateNotFoundException extends RuntimeException {

    CandidateNotFoundException() {
        super("Could not find the candidate. ");
    }
}


