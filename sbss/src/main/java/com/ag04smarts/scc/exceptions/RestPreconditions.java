package com.ag04smarts.scc.exceptions;


public class RestPreconditions {
    public static <T> T checkFound(T resource) {
        if (resource == null) {
            throw new CandidateNotFoundException();
        }
        return resource;
    }
}

