package com.ag04smarts.scc.bootstrap;

import com.ag04smarts.scc.models.Course;
import com.ag04smarts.scc.models.Lecturer;
import com.ag04smarts.scc.repositories.CourseRepository;
import com.ag04smarts.scc.repositories.LecturerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.ag04smarts.scc.enums.CourseType.*;

@Component
public class CourseBootstrap implements ApplicationListener<ContextRefreshedEvent> {

    private CourseRepository courseRepository;
    private LecturerRepository lecturerRepository;

    @Autowired
    public CourseBootstrap(CourseRepository courseRepository, LecturerRepository lecturerRepository) {
        this.courseRepository = courseRepository;
        this.lecturerRepository = lecturerRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        courseRepository.saveAll(getCourses());
    }

    private List<Course> getCourses() {

        List<Course> courses = new ArrayList<>(4);

        Optional<Lecturer> jamieLecturerOptional = lecturerRepository.findByFirstNameAndLastName("Jamie", "Oliver");
        Optional<Lecturer> juliaLecturerOptional = lecturerRepository.findByFirstNameAndLastName("Julia", "Child");
        Optional<Lecturer> gordonLecturerOptional = lecturerRepository.findByFirstNameAndLastName("Gordon", "Ramsay");

        if (jamieLecturerOptional.isEmpty() || juliaLecturerOptional.isEmpty() || gordonLecturerOptional.isEmpty()) {
            throw new RuntimeException("Expected lecturer Not Found");
        }

        Lecturer jamieLecturer = jamieLecturerOptional.get();
        Lecturer juliaLecturer = juliaLecturerOptional.get();
        Lecturer gordonLecturer = gordonLecturerOptional.get();

        Course course1 = new Course();
        course1.setName("Learn to cook");
        course1.setNumberOfStudents(8);
        course1.setType(BASIC);

        Course course2 = new Course();
        course2.setName("Mexican cooking");
        course2.setNumberOfStudents(3);
        course2.setType(BASIC);

        Course course3 = new Course();
        course3.setName("Cooking with style");
        course3.setNumberOfStudents(5);
        course3.setType(INTERMEDIATE);

        Course course4 = new Course();
        course4.setName("Jamie Oliver Cooking course");
        course4.setNumberOfStudents(3);
        course4.setType(ADVANCED);

        course1.getLecturers().add(jamieLecturer);
        course4.getLecturers().add(jamieLecturer);
        course3.getLecturers().add(juliaLecturer);
        course2.getLecturers().add(gordonLecturer);

        courses.add(course1);
        courses.add(course2);
        courses.add(course3);
        courses.add(course4);
        return courses;
    }
}