package com.ag04smarts.scc.bootstrap;

import com.ag04smarts.scc.models.Candidate;
import com.ag04smarts.scc.repositories.CandidateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

/*
@Component
public class CandidateLoader implements ApplicationListener<ContextRefreshedEvent> {

    private CandidateRepository candidateRepository;

    @Autowired
    public CandidateLoader(CandidateRepository candidateRepository) {
        this.candidateRepository = candidateRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        initData();
    }

    private void initData() {

        //Marko
        Candidate marko = new Candidate("Marko", "Gobin", "Zagreb", "0958574756", "marko.gobin@gmail.com", 32);

        //Hana
        Candidate hancica = new Candidate("Hana", "Cepić", "Zagreb", "0958679453", "hana.cepic@gmail.com", 25);

        candidateRepository.save(marko);
        candidateRepository.save(hancica);

        //Ljubica
        Candidate candidate = new Candidate();
        candidate.setName("Ljubica");
        candidate.setSurname("Marković");
        candidate.setAge(30);
        candidate.setCity("Split");
        candidate.setEmail("ljubi30@gmail.com");
        candidate.setNumber("0987423873");

        candidateRepository.save(candidate);
    }
} */