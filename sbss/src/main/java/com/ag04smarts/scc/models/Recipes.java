package com.ag04smarts.scc.models;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class Recipes {

    private List<String> recipes;

    private List<String> lunchRecipes;

    public Recipes() {

        recipes = new ArrayList<>(30);

        recipes.add("recipes by ingredients");
        recipes.add("recipes for dinner");
        recipes.add("recipes for lunch");
        recipes.add("recipes with peanut butter");
        recipes.add("recipes with eggs");
        recipes.add("recipes with tofu");
        recipes.add("recipes with avocado");
        recipes.add("recipes with tortillas");
        recipes.add("recipes with pesto");

        lunchRecipes = new ArrayList<>(30);

        lunchRecipes.add("lunchRecipes by ingredients");
        lunchRecipes.add("lunchRecipes with peanut butter");
        lunchRecipes.add("lunchRecipes with eggs");
        lunchRecipes.add("lunchRecipes with tofu");
        lunchRecipes.add("lunchRecipes with avocado");
        lunchRecipes.add("lunchRecipes with tortillas");
        lunchRecipes.add("lunchRecipes with pesto");
    }

    public String getRandomRecipes() {
        return recipes.get(ThreadLocalRandom.current().nextInt(1, recipes.size()));
    }

    public String getRandomLunchRecipes() {
        return lunchRecipes.get(ThreadLocalRandom.current().nextInt(1, lunchRecipes.size()));
    }

}
