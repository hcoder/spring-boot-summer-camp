package com.ag04smarts.scc.models;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class LunchRecipes {

    private List<String> lunchRecipes;

    public LunchRecipes() {

        lunchRecipes = new ArrayList<>(30);

        lunchRecipes.add("lunchRecipes by ingredients");
        lunchRecipes.add("lunchRecipes with peanut butter");
        lunchRecipes.add("lunchRecipes with eggs");
        lunchRecipes.add("lunchRecipes with tofu");
        lunchRecipes.add("lunchRecipes with avocado");
        lunchRecipes.add("lunchRecipes with tortillas");
        lunchRecipes.add("lunchRecipes with pesto");
    }

    public String getRandomLunchRecipes() {
        return lunchRecipes.get(ThreadLocalRandom.current().nextInt(1, lunchRecipes.size()));
    }
}
