package com.ag04smarts.scc.schoolUrlBeans;

import org.springframework.beans.factory.InitializingBean;

public class HC implements InitializingBean {

    private String apology;

    public String getApology() {
        return apology;
    }

    public void setApology(String apology) {
        this.apology = apology;
    }

    @Override
    public void afterPropertiesSet() {
        System.out.println(apology);
    }
}
