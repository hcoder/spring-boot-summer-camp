package com.ag04smarts.scc.services;

import com.ag04smarts.scc.models.Registration;
import com.ag04smarts.scc.repositories.RegistrationRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.mockito.internal.verification.VerificationModeFactory.times;

public class RegistrationServiceImplTest {

    RegistrationServiceImpl registrationService;

    @Mock
    RegistrationRepository registrationRepository;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        registrationService = new RegistrationServiceImpl(registrationRepository);
    }

    @Test
    public void getAllRegistrations() throws Exception {
        Registration registration = new Registration();
        HashSet registrationsData = new HashSet();
        registrationsData.add(registration);

        when(registrationRepository.findAll()).thenReturn(registrationsData);

        Set<Registration> registrations = registrationService.getAllRegistrations();

        assertEquals(registrations.size(), 1);
        verify(registrationRepository, times(1)).findAll();
    }
}