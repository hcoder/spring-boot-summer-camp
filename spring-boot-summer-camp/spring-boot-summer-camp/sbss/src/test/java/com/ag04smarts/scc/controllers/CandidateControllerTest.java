package com.ag04smarts.scc.controllers;

import com.ag04smarts.scc.services.CandidateService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class CandidateControllerTest {

    @Mock
    CandidateService candidateService;

    private CandidateController candidateController;

    @Before
    public void setUp() throws Exception {

        MockitoAnnotations.initMocks(this);

        candidateController = new CandidateController(candidateService);

    }

    @Test
    public void findAllCandidates() throws Exception {
        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(candidateController).build();

        mockMvc.perform(get("/api/candidate"))
                .andExpect(status().isOk());
    }
}