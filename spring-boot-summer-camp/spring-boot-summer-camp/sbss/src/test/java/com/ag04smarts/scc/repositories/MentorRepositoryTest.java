package com.ag04smarts.scc.repositories;

import com.ag04smarts.scc.models.Mentor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class MentorRepositoryTest {

    @Autowired
    MentorRepository mentorRepository;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void findByFirstNameAndLastName() throws Exception {
        Optional<Mentor> mentorOptional = mentorRepository.findByFirstNameAndLastName("Paula", "Deen");

        assertEquals("Paula", mentorOptional.get().getFirstName());
    }
}