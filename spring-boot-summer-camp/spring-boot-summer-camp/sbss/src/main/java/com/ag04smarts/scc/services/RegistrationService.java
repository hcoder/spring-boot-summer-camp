package com.ag04smarts.scc.services;

import com.ag04smarts.scc.models.Registration;

import java.util.Set;

public interface RegistrationService {

    Registration getRegistrationById(Long id);

    Set<Registration> getAllRegistrations();

    void deleteRegistration(Long id);

    Registration saveRegistration(Registration registration);
}
