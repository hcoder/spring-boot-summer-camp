package com.ag04smarts.scc.exceptions;


public class RestPreconditions {
    public static <T> T checkFound(T resource) {
        if (resource == null) {
            throw new SourceNotFoundException();
        }
        return resource;
    }

    public static <T> T notFound() {
        throw new SourceNotFoundException();
    }

    public static <T> T bookedCourse() {
        throw new CourseBooked();
    }
}

