package com.ag04smarts.scc.exceptions;

public class CourseBooked extends RuntimeException {
    CourseBooked() {
        super("You can not enrol yourself in this course. This course is booked.");
    }
}
