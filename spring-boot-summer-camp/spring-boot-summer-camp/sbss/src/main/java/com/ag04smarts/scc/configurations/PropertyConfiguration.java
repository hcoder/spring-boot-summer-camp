package com.ag04smarts.scc.configurations;

import com.ag04smarts.scc.schoolUrlBeans.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

@Configuration
@PropertySources({
        @PropertySource("classpath:scc.properties"),
        @PropertySource("classpath:scc.yml")
})

public class PropertyConfiguration {

    @Value("${school.url.facebook}")
    String FUrl;

    @Value("${school.url.linkedin}")
    String LUrl;

    @Value("${school.url.twitter}")
    String TUrl;


    @Bean
    public FacebookUrl facebookUrl() {
        FacebookUrl facebookUrl = new FacebookUrl();
        facebookUrl.setUrl(FUrl);
        return facebookUrl;
    }

    @Bean
    public LinkedInUrl linkedInUrl() {
        LinkedInUrl linkedInUrl = new LinkedInUrl();
        linkedInUrl.setUrl(LUrl);
        return linkedInUrl;
    }

    @Bean
    public TwitterUrl twitterUrl() {
        TwitterUrl twitterUrl = new TwitterUrl();
        twitterUrl.setUrl(TUrl);
        return twitterUrl;
    }

    @Bean
    public SCname sCname(@Value("${summer.course.name}") String SCN) {
        SCname sCname = new SCname();
        sCname.setName(SCN);
        return sCname;
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer properties() {
        return new PropertySourcesPlaceholderConfigurer();
    }
}