package com.ag04smarts.scc.services;

import com.ag04smarts.scc.models.Lecturer;
import com.ag04smarts.scc.repositories.LecturerRepository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class LecturerServiceImpl implements LecturerService {


    private final LecturerRepository lecturerRepository;

    public LecturerServiceImpl(LecturerRepository lecturerRepository) {
        this.lecturerRepository = lecturerRepository;
    }

    @Override
    public Lecturer getLecturerById(Long id) {
        return lecturerRepository.findById(id).orElse(null);
    }

    @Override
    public Lecturer saveLecturer(Lecturer lecturer) {
        return lecturerRepository.save(lecturer);
    }

    @Override
    public void deleteLecturer(Long id) {
        lecturerRepository.deleteById(id);
    }

    @Override
    public Set<Lecturer> listAllLecturers() {
        Set<Lecturer> lectureSet = new HashSet<>();
        lecturerRepository.findAll().iterator().forEachRemaining(lectureSet::add);
        return lectureSet;
    }
}
