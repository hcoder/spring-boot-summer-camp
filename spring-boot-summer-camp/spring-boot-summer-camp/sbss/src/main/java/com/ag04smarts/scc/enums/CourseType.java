package com.ag04smarts.scc.enums;

public enum CourseType {
    BASIC, ADVANCED, INTERMEDIATE
}
