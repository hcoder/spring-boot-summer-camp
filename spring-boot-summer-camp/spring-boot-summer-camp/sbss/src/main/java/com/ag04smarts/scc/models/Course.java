package com.ag04smarts.scc.models;

import com.ag04smarts.scc.enums.CourseType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.bytebuddy.build.ToStringPlugin;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@EqualsAndHashCode(exclude = {"lecturers", "registrations"})
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private int numberOfStudents;

    @Enumerated(value = EnumType.STRING)
    private CourseType type;

    @ToStringPlugin.Exclude
    @ManyToMany(mappedBy = "courses")
    private Set<Registration> registrations;

    @ToStringPlugin.Exclude
    @ManyToMany
    @JsonIgnore
    @JoinTable(name = "course_lecturer",
            joinColumns = @JoinColumn(name = "course_id"),
            inverseJoinColumns = @JoinColumn(name = "lecturer_id"))
    private Set<Lecturer> lecturers = new HashSet<>();

    public Course() {
    }

    public Course(String name, int numberOfStudents, CourseType type, Set<Lecturer> lecturers, Set<Registration> registrations) {
        this.name = name;
        this.numberOfStudents = numberOfStudents;
        this.lecturers = lecturers;
        this.type = type;
        this.registrations = registrations;
    }
}
