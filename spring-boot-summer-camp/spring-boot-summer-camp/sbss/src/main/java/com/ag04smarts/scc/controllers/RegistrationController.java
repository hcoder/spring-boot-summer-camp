package com.ag04smarts.scc.controllers;


import com.ag04smarts.scc.commands.CourseCommand;
import com.ag04smarts.scc.commands.RegistrationForm;
import com.ag04smarts.scc.converters.RegistrationFormToRegistration;
import com.ag04smarts.scc.exceptions.RestPreconditions;
import com.ag04smarts.scc.models.Registration;
import com.ag04smarts.scc.services.CourseService;
import com.ag04smarts.scc.services.RegistrationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/api/registration")
public class RegistrationController {

    private RegistrationService registrationService;
    private RegistrationFormToRegistration registrationConverter;
    private CourseService courseService;

    @Autowired
    public RegistrationController(RegistrationService registrationService, RegistrationFormToRegistration registrationConverter, CourseService courseService) {
        this.registrationService = registrationService;
        this.registrationConverter = registrationConverter;
        this.courseService = courseService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Iterable<Registration> findAllRegistrations() {
        return registrationService.getAllRegistrations();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Registration createRegistration(RegistrationForm form) {

        for (CourseCommand courseCommand : form.getCourses()) {
            if (courseCommand.getNumberOfStudents() < courseService.getCourseById(courseCommand.getId()).getRegistrations().size()) {
                RestPreconditions.bookedCourse();
            }
        }

        Registration registration = registrationConverter.convert(form);
        return registrationService.saveRegistration(registration);
    }
}
