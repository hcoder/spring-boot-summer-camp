package com.ag04smarts.scc.commands;

import com.ag04smarts.scc.enums.Gender;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
public class CandidateCommand {

    private Long id;

    @NotNull(message = "This field cannot be null")
    @Size(max = 30)
    private String firstName, lastName, phoneNumber;

    @NotNull(message = "This field cannot be null")
    @Email(message = "Email should be valid")
    private String email;

    @NotNull(message = "This field cannot be null")
    @Min(value = 18, message = "Age should not be less than 18")
    private int age;

    private Gender gender;
    private Byte[] image;
}
