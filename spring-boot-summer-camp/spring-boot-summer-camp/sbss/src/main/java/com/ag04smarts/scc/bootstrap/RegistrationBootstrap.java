package com.ag04smarts.scc.bootstrap;

import com.ag04smarts.scc.enums.CourseType;
import com.ag04smarts.scc.enums.Gender;
import com.ag04smarts.scc.models.Candidate;
import com.ag04smarts.scc.models.Course;
import com.ag04smarts.scc.models.Registration;
import com.ag04smarts.scc.repositories.CandidateRepository;
import com.ag04smarts.scc.repositories.CourseRepository;
import com.ag04smarts.scc.repositories.RegistrationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class RegistrationBootstrap implements ApplicationListener<ContextRefreshedEvent> {

    private CandidateRepository candidateRepository;
    private CourseRepository courseRepository;
    private RegistrationRepository registrationRepository;

    @Autowired
    public RegistrationBootstrap(CandidateRepository candidateRepository, CourseRepository courseRepository, RegistrationRepository registrationRepository) {
        this.candidateRepository = candidateRepository;
        this.courseRepository = courseRepository;
        this.registrationRepository = registrationRepository;
    }

    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {

        try {
            initData();
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    private void initData() throws ParseException {

        List<Registration> registrations = new ArrayList<>(5);
        List<Candidate> candidates = new ArrayList<>(5);

        Candidate candidate1 = candidateRepository.findByFirstName("Lana");
        Candidate candidate2 = candidateRepository.findByFirstName("Karla");
        Candidate candidate3 = candidateRepository.findByFirstName("Sara");
        Candidate candidate4 = candidateRepository.findByFirstName("Ivan");
        Candidate candidate5 = candidateRepository.findByFirstName("Hrvoje");

        Course course1 = courseRepository.findByName("Learn to cook");
        Course course2 = courseRepository.findByName("Mexican cooking");
        Course course3 = courseRepository.findByName("Cooking with style");
        Course course4 = courseRepository.findByName("Jamie Oliver Cooking course");

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        String date1 = "04-08-2019 10:20:56";
        String date2 = "10-07-2019 10:20:56";
        String date3 = "12-07-2019 10:20:56";
        String date4 = "05-08-2019 10:20:56";
        String date5 = "11-08-2019 10:20:56";

        String dateAfterQuery = "20-07-2019 10:20:50";

        Date date11 = sdf.parse(date1);
        Date date22 = sdf.parse(date2);
        Date date33 = sdf.parse(date3);
        Date date44 = sdf.parse(date4);
        Date date55 = sdf.parse(date5);

        Date dateAfter = sdf.parse(dateAfterQuery);

        Registration reg1 = new Registration();
        reg1.setRegistrationDate(date11);
        reg1.setCandidate(candidate1);
        reg1.getCourses().add(course1);

        Registration reg2 = new Registration();
        reg2.setRegistrationDate(date22);
        reg2.setCandidate(candidate2);
        reg2.getCourses().add(course3);

        Registration reg3 = new Registration();
        reg3.setRegistrationDate(date33);
        reg3.setCandidate(candidate3);
        reg3.getCourses().add(course2);

        Registration reg4 = new Registration();
        reg4.setRegistrationDate(date44);
        reg4.setCandidate(candidate4);
        reg4.getCourses().add(course3);

        Registration reg5 = new Registration();
        reg5.setRegistrationDate(date55);
        reg5.setCandidate(candidate5);
        reg5.getCourses().add(course4);

        registrations.add(reg1);
        registrations.add(reg2);
        registrations.add(reg3);
        registrations.add(reg4);
        registrations.add(reg5);

        registrationRepository.saveAll(registrations);

        candidate1.setRegistration(reg1);
        candidate2.setRegistration(reg2);
        candidate3.setRegistration(reg3);
        candidate4.setRegistration(reg4);
        candidate5.setRegistration(reg5);

        candidates.add(candidate1);
        candidates.add(candidate2);
        candidates.add(candidate3);
        candidates.add(candidate4);
        candidates.add(candidate5);

        candidateRepository.saveAll(candidates);

        List<Candidate> candidatesQ = candidateRepository.findByRegistration_RegistrationDateAfterAndAgeGreaterThan(dateAfter, 21);
        for (Candidate candidate : candidatesQ) {
            System.out.println(candidate.getFirstName());
        }

        List<Course> courseList = courseRepository.findByType(CourseType.BASIC);
        for (Course c : courseList) {
            System.out.println(c.getName());
        }

        List<Registration> registrationsQ = registrationRepository.findByCandidate_Gender(Gender.FEMALE);
        for (Registration reg : registrationsQ) {
            System.out.println(reg.getCandidate().getFirstName());
        }

        for (Registration reg : registrationsQ) {
            for (Course c : courseList) {
                if (reg.getCourses().contains(c)) {
                    System.out.println(reg.getCandidate().getFirstName());
                }
            }
        }
    }
}
