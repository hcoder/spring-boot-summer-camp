package com.ag04smarts.scc.repositories;

import com.ag04smarts.scc.enums.Gender;
import com.ag04smarts.scc.models.Candidate;
import com.ag04smarts.scc.models.Course;
import com.ag04smarts.scc.models.Mentor;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface CandidateRepository extends CrudRepository<Candidate, Long> {

    Candidate findByFirstName(String name);

    List<Candidate> findByRegistration_RegistrationDateAfterAndAgeGreaterThan(Date date, int x);
}



