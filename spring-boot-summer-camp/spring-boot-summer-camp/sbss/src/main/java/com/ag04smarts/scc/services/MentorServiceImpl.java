package com.ag04smarts.scc.services;

import com.ag04smarts.scc.models.Mentor;
import com.ag04smarts.scc.repositories.MentorRepository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class MentorServiceImpl implements MentorService {

    private MentorRepository mentorRepository;

    public MentorServiceImpl(MentorRepository mentorRepository) {
        this.mentorRepository = mentorRepository;
    }

    @Override
    public Mentor getMentorById(Long id) {
        return mentorRepository.findById(id).orElse(null);
    }

    @Override
    public Mentor saveMentor(Mentor mentor) {
        return mentorRepository.save(mentor);
    }

    @Override
    public void deleteMentor(Long id) {
        mentorRepository.deleteById(id);
    }

    @Override
    public Set<Mentor> getAllMentors() {
        Set<Mentor> mentors = new HashSet<>();
        mentorRepository.findAll().iterator().forEachRemaining(mentors::add);
        return mentors;
    }
}
