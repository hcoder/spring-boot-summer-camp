package com.ag04smarts.scc.services;

import com.ag04smarts.scc.models.Registration;
import com.ag04smarts.scc.repositories.RegistrationRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Slf4j
@Service
public class RegistrationServiceImpl implements RegistrationService {

    private RegistrationRepository registrationRepository;

    public RegistrationServiceImpl(RegistrationRepository registrationRepository) {
        this.registrationRepository = registrationRepository;
    }

    @Override
    public Registration getRegistrationById(Long id) {
        return registrationRepository.findById(id).orElse(null);
    }

    @Override
    public Set<Registration> getAllRegistrations() {
        log.debug("Fine, in service.");
        Set<Registration> registrations = new HashSet<>();
        registrationRepository.findAll().iterator().forEachRemaining(registrations::add);
        return registrations;
    }

    @Override
    public void deleteRegistration(Long id) {
        registrationRepository.deleteById(id);
    }

    @Override
    public Registration saveRegistration(Registration registration) {
        return registrationRepository.save(registration);
    }
}
