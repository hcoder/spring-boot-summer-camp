package com.ag04smarts.scc.repositories;

import com.ag04smarts.scc.enums.CourseType;
import com.ag04smarts.scc.models.Course;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CourseRepository extends CrudRepository<Course, Long> {

    Course findByName(String name);

    List<Course> findByType(CourseType courseType);
}
