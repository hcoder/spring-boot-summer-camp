package com.ag04smarts.scc.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.bytebuddy.build.ToStringPlugin;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@EqualsAndHashCode(exclude = {"courses", "candidate"})
public class Registration {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Date registrationDate;

    @ToStringPlugin.Exclude
    @ManyToMany
    @JsonIgnore
    @JoinTable(name = "registration_course",
            joinColumns = @JoinColumn(name = "registration_id"),
            inverseJoinColumns = @JoinColumn(name = "course_id"))
    private Set<Course> courses = new HashSet<>();

    @ToStringPlugin.Exclude
    @OneToOne
    private Candidate candidate;

    public Registration() {
    }

    public Registration(Date registrationDate, Set<Course> courses, Candidate candidate) {
        this.registrationDate = registrationDate;
        this.candidate = candidate;
        this.courses = courses;
    }
}
