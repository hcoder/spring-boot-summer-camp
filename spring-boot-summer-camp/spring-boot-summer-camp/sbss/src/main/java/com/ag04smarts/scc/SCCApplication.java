package com.ag04smarts.scc;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class SCCApplication {

    public static void main(String[] args) {

        ApplicationContext ctx = SpringApplication.run(SCCApplication.class, args);

    }
}
