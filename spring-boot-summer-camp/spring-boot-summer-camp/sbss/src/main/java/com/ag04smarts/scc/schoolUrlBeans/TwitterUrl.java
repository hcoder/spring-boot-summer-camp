package com.ag04smarts.scc.schoolUrlBeans;

public class TwitterUrl {

    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
