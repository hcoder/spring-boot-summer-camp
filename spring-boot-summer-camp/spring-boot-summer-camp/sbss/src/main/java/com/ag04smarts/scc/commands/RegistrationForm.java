package com.ag04smarts.scc.commands;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.FutureOrPresent;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;


@Getter
@Setter
@NoArgsConstructor
public class RegistrationForm {

    private Long id;

    @FutureOrPresent
    private Date registrationDate;

    private Set<CourseCommand> courses = new HashSet<>();
    private CandidateCommand candidate;

}
