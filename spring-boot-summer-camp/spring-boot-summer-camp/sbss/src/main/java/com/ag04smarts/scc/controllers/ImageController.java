package com.ag04smarts.scc.controllers;

import com.ag04smarts.scc.commands.CandidateCommand;
import com.ag04smarts.scc.exceptions.RestPreconditions;
import com.ag04smarts.scc.models.Candidate;
import com.ag04smarts.scc.services.CandidateService;
import com.ag04smarts.scc.services.ImageService;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

@Controller
public class ImageController {

    private final ImageService imageService;
    private final CandidateService candidateService;

    public ImageController(ImageService imageService, CandidateService candidateService) {
        this.imageService = imageService;
        this.candidateService = candidateService;
    }

    @GetMapping("candidate/{id}/image")
    @ResponseStatus(HttpStatus.OK)
    public Candidate showUploadForm(@PathVariable String id) {
        return RestPreconditions.checkFound(candidateService.getCandidateById(Long.valueOf(id)));
    }

    @PostMapping("candidate/{id}/image")
    @ResponseStatus(HttpStatus.CREATED)
    public void handleImagePost(@PathVariable String id, @RequestParam("imagefile") MultipartFile file) {
        imageService.saveImageFile(Long.valueOf(id), file);
    }

    @GetMapping("candidate/{id}/candidateimage")
    @ResponseStatus(HttpStatus.OK)
    public void renderImageFromDB(@PathVariable String id, HttpServletResponse response) throws IOException {
        CandidateCommand candidateCommand = candidateService.findCommandById(Long.valueOf(id));

        if (candidateCommand.getImage() != null) {
            byte[] byteArray = new byte[candidateCommand.getImage().length];
            int i = 0;

            for (Byte wrappedByte : candidateCommand.getImage()) {
                byteArray[i++] = wrappedByte; //auto unboxing
            }

            response.setContentType("image/jpeg");
            InputStream is = new ByteArrayInputStream(byteArray);
            IOUtils.copy(is, response.getOutputStream());
        }
    }
}