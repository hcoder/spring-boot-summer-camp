package com.ag04smarts.scc.services;

import com.ag04smarts.scc.models.Course;
import com.ag04smarts.scc.repositories.CourseRepository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class CourseServiceImpl implements CourseService {

    private final CourseRepository courseRepository;

    public CourseServiceImpl(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }

    @Override
    public Course getCourseById(Long id) {
        return courseRepository.findById(id).orElse(null);
    }

    @Override
    public Course saveCourse(Course course) {
        return courseRepository.save(course);
    }

    @Override
    public void deleteCourse(Long id) {
        courseRepository.deleteById(id);
    }

    @Override
    public Set<Course> listAllCourses() {
        Set<Course> courseSet = new HashSet<>();
        courseRepository.findAll().iterator().forEachRemaining(courseSet::add);
        return courseSet;
    }
}
