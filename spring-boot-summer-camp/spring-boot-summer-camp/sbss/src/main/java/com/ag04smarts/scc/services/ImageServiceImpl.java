package com.ag04smarts.scc.services;

import com.ag04smarts.scc.models.Candidate;
import com.ag04smarts.scc.repositories.CandidateRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Slf4j
@Service
public class ImageServiceImpl implements ImageService {

    private final CandidateRepository candidateRepository;

    public ImageServiceImpl(CandidateRepository candidateRepository) {
        this.candidateRepository = candidateRepository;
    }

    @Override
    @Transactional
    public void saveImageFile(Long candidateId, MultipartFile file) {

        try {
            Candidate candidate = candidateRepository.findById(candidateId).get();

            Byte[] byteObjects = new Byte[file.getBytes().length];

            int i = 0;

            for (byte b : file.getBytes()) {
                byteObjects[i++] = b;
            }

            candidate.setImage(byteObjects);

            candidateRepository.save(candidate);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
