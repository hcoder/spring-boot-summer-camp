package com.ag04smarts.scc.repositories;

import com.ag04smarts.scc.enums.Gender;
import com.ag04smarts.scc.models.Candidate;
import com.ag04smarts.scc.models.Course;
import com.ag04smarts.scc.models.Registration;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Set;

public interface RegistrationRepository extends CrudRepository<Registration, Long> {

    List<Registration> findByCandidate_Gender(Gender gender);
}
