package com.ag04smarts.scc.schoolUrlBeans;

import org.springframework.beans.factory.InitializingBean;

public class FacebookUrl implements InitializingBean {

    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public void afterPropertiesSet() {
        System.out.println(url);
    }
}
