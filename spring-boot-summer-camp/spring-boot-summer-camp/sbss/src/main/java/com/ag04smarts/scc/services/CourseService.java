package com.ag04smarts.scc.services;

import com.ag04smarts.scc.models.Course;

import java.util.Set;

public interface CourseService {

    Course getCourseById(Long id);

    Course saveCourse(Course course);

    void deleteCourse(Long id);

    Set<Course> listAllCourses();
}
