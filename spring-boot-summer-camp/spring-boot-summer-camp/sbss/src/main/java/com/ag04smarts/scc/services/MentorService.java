package com.ag04smarts.scc.services;

import com.ag04smarts.scc.models.Mentor;

import java.util.Set;

public interface MentorService {

    Mentor getMentorById(Long id);

    Mentor saveMentor(Mentor mentor);

    void deleteMentor(Long id);

    Set<Mentor> getAllMentors();
}
