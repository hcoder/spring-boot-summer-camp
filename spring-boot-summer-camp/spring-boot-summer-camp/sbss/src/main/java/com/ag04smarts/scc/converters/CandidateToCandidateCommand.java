package com.ag04smarts.scc.converters;

import com.ag04smarts.scc.commands.CandidateCommand;
import com.ag04smarts.scc.models.Candidate;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class CandidateToCandidateCommand implements Converter<Candidate, CandidateCommand> {

    @Synchronized
    @Nullable
    @Override
    public CandidateCommand convert(Candidate source) {
        if (source == null) {
            return null;
        }

        final CandidateCommand candidate = new CandidateCommand();
        candidate.setId(source.getId());
        candidate.setFirstName(source.getFirstName());
        candidate.setLastName(source.getLastName());
        candidate.setPhoneNumber(source.getPhoneNumber());
        candidate.setAge(source.getAge());
        candidate.setEmail(source.getEmail());
        candidate.setGender(source.getGender());
        candidate.setImage(source.getImage());

        return candidate;
    }
}
