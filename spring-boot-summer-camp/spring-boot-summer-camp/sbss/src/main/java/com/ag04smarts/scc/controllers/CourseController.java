package com.ag04smarts.scc.controllers;

import com.ag04smarts.scc.exceptions.RestPreconditions;
import com.ag04smarts.scc.models.Course;
import com.ag04smarts.scc.services.CourseService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@Slf4j
@RestController
@RequestMapping("/api/course")
public class CourseController {

    private CourseService courseService;

    @Autowired
    public CourseController(CourseService courseService) {
        this.courseService = courseService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Set<Course> findAllCourses() {
        return courseService.listAllCourses();
    }

    @GetMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Course findCourseById(@PathVariable("id") Long id) {
        log.debug("finding course");
        return RestPreconditions.checkFound(courseService.getCourseById(id));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Course createCourse(Course course) {
        return courseService.saveCourse(course);
    }

    @PutMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void updateCourse(@PathVariable("id") Long id, Course course) {
        RestPreconditions.checkFound(courseService.getCourseById(id));
        courseService.saveCourse(course);
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable("id") Long id) {
        courseService.deleteCourse(id);
    }

}