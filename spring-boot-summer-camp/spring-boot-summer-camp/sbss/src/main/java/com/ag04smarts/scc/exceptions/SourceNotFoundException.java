package com.ag04smarts.scc.exceptions;


public class SourceNotFoundException extends RuntimeException {

    SourceNotFoundException() {
        super("Could not find the source.");
    }
}


