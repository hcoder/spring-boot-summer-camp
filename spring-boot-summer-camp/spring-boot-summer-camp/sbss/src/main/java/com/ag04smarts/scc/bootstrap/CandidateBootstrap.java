package com.ag04smarts.scc.bootstrap;

import com.ag04smarts.scc.models.Candidate;
import com.ag04smarts.scc.models.Mentor;
import com.ag04smarts.scc.repositories.CandidateRepository;
import com.ag04smarts.scc.repositories.MentorRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.*;

import static com.ag04smarts.scc.enums.Gender.FEMALE;
import static com.ag04smarts.scc.enums.Gender.MALE;

@Slf4j
@Component
public class CandidateBootstrap implements ApplicationListener<ContextRefreshedEvent> {

    private CandidateRepository candidateRepository;
    private MentorRepository mentorRepository;

    @Autowired
    public CandidateBootstrap(CandidateRepository candidateRepository, MentorRepository mentorRepository) {
        this.candidateRepository = candidateRepository;
        this.mentorRepository = mentorRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        candidateRepository.saveAll(getCandidates());
        log.debug("Yellow candidates data !");
    }

    private List<Candidate> getCandidates() {

        List<Candidate> candidates = new ArrayList<>(5);

        Optional<Mentor> mentor2Optional = mentorRepository.findByFirstNameAndLastName("Anthony", "Bourdain");
        Optional<Mentor> mentor1Optional = mentorRepository.findByFirstNameAndLastName("Rachael", "Ray");
        Optional<Mentor> mentor3Optional = mentorRepository.findByFirstNameAndLastName("Giada", "De Laurentiis");
        Optional<Mentor> mentor4Optional = mentorRepository.findByFirstNameAndLastName("Paula", "Deen");

        if (mentor1Optional.isEmpty() || mentor2Optional.isEmpty() || mentor3Optional.isEmpty() || mentor4Optional.isEmpty()) {
            throw new RuntimeException("Expected mentor Not Found");
        }

        Mentor mentor1 = mentor1Optional.get();
        Mentor mentor2 = mentor2Optional.get();
        Mentor mentor3 = mentor3Optional.get();
        Mentor mentor4 = mentor4Optional.get();

        Candidate candidate1 = new Candidate();
        candidate1.setFirstName("Lana");
        candidate1.setLastName("Marković");
        candidate1.setEmail("lana.markovic@gmail.com");
        candidate1.setAge(24);
        candidate1.setPhoneNumber("0982354768");
        candidate1.setGender(FEMALE);

        Candidate candidate2 = new Candidate();
        candidate2.setFirstName("Karla");
        candidate2.setLastName("Leskovar");
        candidate2.setEmail("karla.leskovar@gmail.com");
        candidate2.setAge(20);
        candidate2.setPhoneNumber("0978694578");
        candidate2.setGender(FEMALE);

        Candidate candidate3 = new Candidate();
        candidate3.setFirstName("Sara");
        candidate3.setLastName("Buljan");
        candidate3.setEmail("sara.buljan@gmail.com");
        candidate3.setAge(23);
        candidate3.setPhoneNumber("0997582346");
        candidate3.setGender(FEMALE);

        Candidate candidate4 = new Candidate();
        candidate4.setFirstName("Ivan");
        candidate4.setLastName("Rogić");
        candidate4.setEmail("ivan.rogic@gmail.com");
        candidate4.setAge(32);
        candidate4.setPhoneNumber("0957689346");
        candidate4.setGender(MALE);

        Candidate candidate5 = new Candidate();
        candidate5.setFirstName("Hrvoje");
        candidate5.setLastName("Capan");
        candidate5.setEmail("hrvoje.capan@gmail.com");
        candidate5.setAge(31);
        candidate5.setPhoneNumber("0957689213");
        candidate5.setGender(MALE);

        candidate1.setMentor(mentor1);
        candidate2.setMentor(mentor2);
        candidate3.setMentor(mentor3);
        candidate4.setMentor(mentor4);
        candidate5.setMentor(mentor1);

        candidates.add(candidate1);
        candidates.add(candidate2);
        candidates.add(candidate3);
        candidates.add(candidate4);
        candidates.add(candidate5);

        return candidates;
    }
}
