package com.ag04smarts.scc.models;

import com.ag04smarts.scc.enums.Gender;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.bytebuddy.build.ToStringPlugin;

import javax.persistence.*;

@Entity
@Data
@EqualsAndHashCode(exclude = {"mentor", "registration"})
public class Candidate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String firstName, lastName, phoneNumber, email;
    private int age;

    @Enumerated(value = EnumType.STRING)
    private Gender gender;

    @ToStringPlugin.Exclude
    @ManyToOne
    @JsonIgnore
    private Mentor mentor;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "candidate", optional = false)
    @JsonIgnore
    private Registration registration;

    @Lob
    private Byte[] image;

    public Candidate() {
    }

    public Candidate(String firstName, String lastName, String phoneNumber, String email, int age, Mentor mentor, Registration registration) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.age = age;
        this.registration = registration;
        this.mentor = mentor;
    }
}

