package com.ag04smarts.scc.models;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.bytebuddy.build.ToStringPlugin;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@EqualsAndHashCode(exclude = {"courses"})
public class Lecturer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String firstName;
    private String lastName;

    @ToStringPlugin.Exclude
    @ManyToMany(mappedBy = "lecturers")
    private Set<Course> courses;

    public Lecturer() {
    }

    public Lecturer(String firstName, String lastName, Set<Course> courses) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.courses = courses;
    }
}
