package com.ag04smarts.scc.services;

import com.ag04smarts.scc.models.Lecturer;

import java.util.Set;

public interface LecturerService {

    Lecturer getLecturerById(Long id);

    Lecturer saveLecturer(Lecturer lecturer);

    void deleteLecturer(Long id);

    Set<Lecturer> listAllLecturers();
}
