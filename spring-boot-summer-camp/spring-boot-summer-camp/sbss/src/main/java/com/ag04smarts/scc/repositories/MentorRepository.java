package com.ag04smarts.scc.repositories;

import com.ag04smarts.scc.models.Mentor;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface MentorRepository extends CrudRepository<Mentor, Long> {
    Optional<Mentor> findByFirstNameAndLastName(String name, String surname);
}
