package com.ag04smarts.scc.commands;

import com.ag04smarts.scc.models.Course;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class LecturerCommand {
    private Long id;

    private String firstName;
    private String lastName;
    private Set<Course> courses;
}
