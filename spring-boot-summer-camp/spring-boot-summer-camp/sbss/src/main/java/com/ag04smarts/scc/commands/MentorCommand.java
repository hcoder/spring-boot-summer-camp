package com.ag04smarts.scc.commands;

import com.ag04smarts.scc.models.Candidate;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class MentorCommand {
    private Long id;

    private String firstName;
    private String lastName;
    private Set<Candidate> candidates;
}
