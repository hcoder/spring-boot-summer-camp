package com.ag04smarts.scc.services;

import com.ag04smarts.scc.commands.CandidateCommand;
import com.ag04smarts.scc.models.Candidate;

public interface CandidateService {

    Iterable<Candidate> listAllCandidates();

    Candidate getCandidateById(Long id);

    Candidate saveCandidate(Candidate candidate);

    void deleteCandidate(Long id);

    CandidateCommand findCommandById(Long l);
}


