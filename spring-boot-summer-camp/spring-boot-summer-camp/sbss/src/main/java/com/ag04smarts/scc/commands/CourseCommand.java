package com.ag04smarts.scc.commands;

import com.ag04smarts.scc.enums.CourseType;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
public class CourseCommand {

    private Long id;

    @NotNull
    @Size(max=30)
    private String name;

    private int numberOfStudents;

    private CourseType type;

}
