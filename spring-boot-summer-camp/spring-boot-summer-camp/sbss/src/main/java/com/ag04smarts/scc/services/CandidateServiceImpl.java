package com.ag04smarts.scc.services;

import com.ag04smarts.scc.commands.CandidateCommand;
import com.ag04smarts.scc.converters.CandidateCommandToCandidate;
import com.ag04smarts.scc.converters.CandidateToCandidateCommand;
import com.ag04smarts.scc.models.Candidate;
import com.ag04smarts.scc.repositories.CandidateRepository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CandidateServiceImpl implements CandidateService {

    private CandidateRepository candidateRepository;
    private CandidateToCandidateCommand candidateToCandidateCommand;

    public CandidateServiceImpl(CandidateRepository candidateRepository, CandidateToCandidateCommand candidateToCandidateCommand) {
        this.candidateRepository = candidateRepository;
        this.candidateToCandidateCommand = candidateToCandidateCommand;
    }

    @Override
    public Iterable<Candidate> listAllCandidates() {
        return candidateRepository.findAll();
    }

    @Override
    public Candidate getCandidateById(Long id) {

        return candidateRepository.findById(id).orElse(null);
    }

    @Override
    public Candidate saveCandidate(Candidate candidate) {
        return candidateRepository.save(candidate);
    }

    @Override
    public void deleteCandidate(Long id) {
        candidateRepository.deleteById(id);
    }

    @Override
    @Transactional
    public CandidateCommand findCommandById(Long l) {
        return candidateToCandidateCommand.convert(getCandidateById(l));
    }

}


