package com.ag04smarts.scc.models;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.bytebuddy.build.ToStringPlugin;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@EqualsAndHashCode(exclude = {"candidates"})
public class Mentor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String firstName;
    private String lastName;

    @ToStringPlugin.Exclude
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "mentor")
    private Set<Candidate> candidates= new HashSet<>();

    public Mentor() {
    }

    public Mentor(String firstName, String lastName, Set<Candidate> candidates) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.candidates = candidates;
    }
}
