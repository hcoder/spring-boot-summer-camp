package com.ag04smarts.scc.controllers;

import com.ag04smarts.scc.exceptions.RestPreconditions;
import com.ag04smarts.scc.exceptions.SourceNotFoundException;
import com.ag04smarts.scc.models.Candidate;
import com.ag04smarts.scc.services.CandidateService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/api/candidate")
public class CandidateController {

    private CandidateService candidateService;

    @Autowired
    public CandidateController(CandidateService candidateService) {
        this.candidateService = candidateService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Iterable<Candidate> findAllCandidates() {
        log.debug("finding candidates");
        return candidateService.listAllCandidates();
    }


    @GetMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Candidate findCandidateById(@PathVariable("id") Long id) {
        return RestPreconditions.checkFound(candidateService.getCandidateById(id));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Candidate createCandidate(Candidate candidate) {
        return candidateService.saveCandidate(candidate);
    }


    @PutMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void updateCandidate(@PathVariable("id") Long id, Candidate candidate) {
        RestPreconditions.checkFound(candidateService.getCandidateById(id));
        candidateService.saveCandidate(candidate);
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteCandidate(@PathVariable("id") Long id) {
        candidateService.deleteCandidate(id);
    }


    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(ChangeSetPersister.NotFoundException.class)
    public void handleNotFound() {
        log.error("Handling not found exception");
        RestPreconditions.notFound();
    }
}




