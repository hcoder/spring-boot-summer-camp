package com.ag04smarts.scc.converters;

import com.ag04smarts.scc.commands.CourseCommand;
import com.ag04smarts.scc.models.Course;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class CourseCommandToCourse implements Converter<CourseCommand, Course> {

    @Synchronized
    @Nullable
    @Override
    public Course convert(CourseCommand source) {
        if (source == null) {
            return null;
        }

        final Course course = new Course();
        course.setId(source.getId());
        course.setName(source.getName());
        course.setNumberOfStudents(source.getNumberOfStudents());
        course.setType(source.getType());

        return course;
    }
}


