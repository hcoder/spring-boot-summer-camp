package com.ag04smarts.scc.converters;

import com.ag04smarts.scc.models.Registration;
import com.ag04smarts.scc.commands.RegistrationForm;
import com.ag04smarts.scc.services.CandidateService;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class RegistrationFormToRegistration implements Converter<RegistrationForm, Registration> {


    private final CandidateCommandToCandidate candidateConverter;
    private final CourseCommandToCourse courseConverter;
    private final CandidateService candidateService;

    public RegistrationFormToRegistration(CandidateCommandToCandidate candidateConverter, CourseCommandToCourse courseConverter, CandidateService candidateService) {
        this.candidateConverter = candidateConverter;
        this.courseConverter = courseConverter;
        this.candidateService = candidateService;
    }

    @Synchronized
    @Nullable
    @Override
    public Registration convert(RegistrationForm source) {
        if (source == null) {
            return null;
        }

        final Registration registration = new Registration();
        registration.setId(source.getId());
        registration.setRegistrationDate(source.getRegistrationDate());
        registration.setCandidate(candidateConverter.convert(source.getCandidate()));

        candidateService.saveCandidate(candidateConverter.convert(source.getCandidate()));

        if (source.getCourses() != null && source.getCourses().size() > 0) {
            source.getCourses()
                    .forEach(course -> registration.getCourses().add(courseConverter.convert(course)));
        }

        return registration;
    }
}